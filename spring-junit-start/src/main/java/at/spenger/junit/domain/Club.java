package at.spenger.junit.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Club {
	private List<Person> l;
	
	public Club() {
		l = new ArrayList<>();
	}
	
	public boolean enter(Person p) {
		if (p == null) {
			throw new IllegalArgumentException("person is null!");
		}
		return l.add(p);
	}
	
	public int numberOf() {
		return l.size();
	}
	
	public List<Person> getPersons() {
		return Collections.unmodifiableList(l);
	}

}
